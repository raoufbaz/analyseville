/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problematique;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author raouf
 */
public class Main {
    
    public static void main(String[] args) {
       boolean startQuartier = false;
       boolean startMenage = false;
       boolean fix =false;
       int comma=0;
       
       
       Ville ville = new Ville();
       Quartier quartier = new Quartier();
       
       try {
        DataInputStream dis = null;
        dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("data01.txt"))));
        while(dis.available() > 0){
            String line = dis.readLine();
            //SAVE QUARTIER NAME
            if (startQuartier == false) {
            quartier.setNom(line);
            startQuartier = true;
            }
            //START OR FINISH MENAGE LIST
            if (line.contains(";")) {
              comma++;
                if(comma%2==0) {
                    startQuartier=false;
                    startMenage=false;
                    ArrayList<Menage> listeMenages = new ArrayList<Menage>(quartier.listeMenages);
                    Quartier q1 = new Quartier();
                     q1.setNom(quartier.getNom());
                     q1.setListeMenages(listeMenages);
                    ville.listeQuartiers.add(q1);
                   quartier.listeMenages.clear();
                }
                else{
                startMenage=true;
                fix=true;
                }
            }
            //SAVE MENAGE
            if (startMenage==true && fix==false) {
                String[]m1 = line.split("\\s+");
                Menage menage = new Menage(m1[0],Double.parseDouble(m1[1]),Integer.parseInt(m1[2])); 
                quartier.listeMenages.add(menage);
            }
            fix=false;

        //System.out.println(line);
        }
    } catch (FileNotFoundException ex) {
        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    }
    
      // System.out.println(ville.listeQuartiers.get(1).listeMenages.get(0).getNBPersonne());
      //System.out.println(ville.listeQuartiers.get(0).listeMenages.get(9).sousLeSeuil());
      //System.out.println(ville.listeQuartiers.get(1).getRevenuMoyen());
      //System.out.println(ville.listeQuartiers.get(1).getPourcentageSeuil());
      //System.out.println(ville.getRevenuMoyen());
      //System.out.println(ville.getSeuilTotal());
    
    
  DataOutputStream dos;
             try {                                                                                                      // si le fichier txt existe pas, il va le creer
                dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File("result.txt")))); //true for Append (start writing at the end of file)
                    dos.writeBytes("a) Revenu Moyen:" + "\t" + (int) ville.getRevenuMoyen() + "\n");
                    dos.writeBytes("b) Liste des quartiers defavorables." + "\n");
                    for (int i = 0; i < ville.QuartierDefavorise.size(); i++) {
                    dos.writeBytes(ville.QuartierDefavorise.get(i).getNom()+".  Revenu moyen: " +(int)ville.QuartierDefavorise.get(i).getRevenuMoyen()+ "\n");
                 }
                    dos.writeBytes("c) \n");
                for(Quartier var: ville.PlusPetit)
                     dos.writeBytes(var.getNom()+". Quartier avec le plus petit revenu moyen: \t"+(int)var.getRevenuMoyen() + "\n");
                for(Quartier var: ville.PlusGrand)
                     dos.writeBytes(var.getNom()+". Quartier avec le plus Grand revenu moyen: \t"+(int)var.getRevenuMoyen() + "\n");
                
                dos.writeBytes("d) Pourcentage de menages dont le revenu est faible: " +(int)ville.getSeuilTotal()+"%" +  "\n");
                
                dos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
      
      
      
    }
}
