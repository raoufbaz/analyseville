/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problematique;

/**
 *
 * @author Admin
 */
public class Menage {
 private String Id;
 private double Revenu;
 private int nbPersonne;
private boolean sousLeSeuil;
 
 public Menage(String Id, double Revenu, int nbPersonne){
     this.Id=Id;
     this.Revenu=Revenu;
     this.nbPersonne=nbPersonne;
     
     double seuil = 7000 + 850 * (nbPersonne -2);
     if (this.Revenu > seuil)
         sousLeSeuil=false;
     else
         sousLeSeuil=true;
 }

 public void setId(String id){this.Id=id;}
public String getId(){return Id;}

 public void setRevenu(double revenu){this.Revenu=revenu;}
public double getRevenu(){return Revenu;}

public void setNBPersonne(int nb){this.nbPersonne=nb;}
public int getNBPersonne(){return this.nbPersonne;}

public boolean sousLeSeuil(){return this.sousLeSeuil;}


}
