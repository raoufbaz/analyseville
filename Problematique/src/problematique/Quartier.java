/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problematique;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class Quartier {
   private String Nom;
    private double revenuMoyen;
    private double pourcentageSeuil;
    public ArrayList<Menage> listeMenages = new ArrayList<Menage>();


    public Quartier(){};
    
    public void setNom(String nom){this.Nom = nom;}
    public String getNom(){return Nom;}
    
    public double getRevenuMoyen(){
        DecimalFormat df = new DecimalFormat("#.##");      
        return Double.valueOf(df.format(revenuMoyen));
    }
    
    public double getPourcentageSeuil(){
         DecimalFormat df = new DecimalFormat("#.##");      
        return Double.valueOf(df.format(pourcentageSeuil));}
            
    public void setListeMenages(ArrayList<Menage> listeMenages){
        this.listeMenages=listeMenages;
        CalculerRevenuMoyen(listeMenages);
        CalculerPourcentageSeuil(listeMenages);
    }

    private void CalculerRevenuMoyen(ArrayList<Menage> liste){
        double total = 0;
        for (int i = 0; i < liste.size(); i++)
           total+=liste.get(i).getRevenu();
        this.revenuMoyen= total/liste.size();
    }
     
    private void CalculerPourcentageSeuil(ArrayList<Menage> liste){
        int total = 0;
        for (int i = 0; i < liste.size(); i++) 
            if (liste.get(i).sousLeSeuil()==true)
                total++;
        this.pourcentageSeuil= (total*100)/liste.size();
    }
}
